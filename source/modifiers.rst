Modifiers
=========
|since_itg|

.. contents:: :local:

| This list is complete (as of |notitg_v4_0_1|), but sorely lacking in descriptions/images. 
| Want to help contribute to these docs? The source is available at https://gitlab.com/CraftedCart/notitg_docs

| All # are ints, example: spline#rotx# can be spline1rotx3
| All % are floats, example: %x can be 1.5x

<noteskin>
----------
where ``<noteskin>`` is the name of a noteskin (example: ``metal``)

|since_itg|

| Sets the player's noteskin.
| This modifier does not seem to work via :lua:meth:`GameState.ApplyModifiers` - use :lua:meth:`GameState.LaunchAttack`, or a ``#MODS``/``#ATTACKS`` section in your .sm file.

%x
--
|since_itg|

| Sets the scroll speed to use xMod at a given speed.
| With no other modifiers, 1x causes arrows to scroll up 1/10th of the screen (the size of the arrow sprite) per beat.

AddScore
--------
|since_itg|

Sets the score display to add mode (default).

Alternate / Ultraman
--------------------
|since_itg|

| Like `Reverse`_, but only affecting the second and forth column.
| Negative percentages affect the first and third column instead.

| For more complicated mods, consider using column specific Reverse instead.
| Test this and similar mods at https://fms-cat.com/flip-invert/

ApproachType
------------
|since_notitg_v2|

ArrowCull
---------
|since_notitg_v3_1|

ArrowPath
---------
| |since_notitg_v1|
| |column_specific_available|

Shows lines for the path arrows take to the receptors

ArrowPathDiffuse|%|%|%
^^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

ArrowPathDrawDistance
^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

ArrowPathDrawDistanceBack
^^^^^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

ArrowPathDrawDistanceFront
^^^^^^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

ArrowPathDrawSize
^^^^^^^^^^^^^^^^^
|since_notitg_v1|

ArrowPathDrawSizeBack
^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

ArrowPathDrawSizeFront
^^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

ArrowPathFadeBottom|%|%|%
^^^^^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

ArrowPathFadeBottomOffset|%|%|%
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

ArrowPathFadeTop|%|%|%
^^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

ArrowPathFadeTopOffset|%|%|%
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

ArrowPathGrain / ArrowPathGranulate
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

ArrowPathSize / ArrowPathGirth
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

ArrowPathWidth
^^^^^^^^^^^^^^
|since_notitg_v1|

Asymptote
---------
|since_notitg_v2|

AsymptoteOffset
^^^^^^^^^^^^^^^
|since_notitg_v2|

AsymptoteScale
^^^^^^^^^^^^^^
|since_notitg_v2|

AsymptoteSize
^^^^^^^^^^^^^
|since_notitg_v2|

Attenuate / AttenuateX
----------------------
|since_notitg_v1|

AttenuateOffset / AttenuateXOffset
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v2|

AttenuateZ
----------
|since_notitg_v3|

AttenuateZOffset
^^^^^^^^^^^^^^^^
|since_notitg_v3|

AverageScore
------------
|since_itg|

Sets the score display to average mode.

Beat
----
| |since_itg|
| |column_specific_available_since_notitg_v4_2_0|

Shakes the receptors and arrows left and right on every beat.

BeatCap
^^^^^^^
| |since_notitg_v2|
| |column_specific_available_since_notitg_v4_2_0|

BeatMult
^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

BeatOffset
^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

BeatPeriod
^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

BeatSize
^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

BeatY
-----
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

BeatCapY
^^^^^^^^
| |since_notitg_v2|
| |column_specific_available_since_notitg_v4_2_0|

BeatYMult
^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

BeatYOffset
^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

BeatYPeriod
^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

BeatYSize
^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

BeatZ
-----
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

BeatCapZ
^^^^^^^^
| |since_notitg_v2|
| |column_specific_available_since_notitg_v4_2_0|

BeatZMult
^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

BeatZOffset
^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

BeatZPeriod
^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

BeatZSize
^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

Big
---
|since_itg|

Adds 8th notes between 4th notes.

Blind
-----
|since_itg|

| Hides the combo, judgements and causes all noteflashes to appear fantastic, removing all player feedback.
| With negative percentages, it only hides the combo.

It's recommended hiding judgements with actor:hidden().

Blink
-----
|since_itg|

Flashes the arrows between visable and hidden.

BlinkBlue / BlinkB
------------------
|since_notitg_v1|

BlinkGreen / BlinkG
-------------------
|since_notitg_v1|

BlinkRed / BlinkR
-----------------
|since_notitg_v1|

BMRize
------
|since_itg|

Combines `Big`_ and `Quick`_.

Boomerang
---------
|since_itg|

Makes the arrows approach from beyond the receptors, scrolling in the opposite direction before turning back.

Boost
-----
| |since_itg|
| |column_specific_available_since_notitg_v4_2_0|

Speeds up the arrows as they approach the receptors.

Bounce
------
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

BounceOffset
^^^^^^^^^^^^
|since_notitg_v1|

BouncePeriod
^^^^^^^^^^^^
|since_notitg_v1|

BounceZ
-------
|since_notitg_v1|

BounceZOffset
^^^^^^^^^^^^^
|since_notitg_v1|

BounceZPeriod
^^^^^^^^^^^^^
|since_notitg_v1|

Brake / Land
------------
| |since_itg|
| |column_specific_available_since_notitg_v4_2_0|

Slows down the arrows as they approach the receptors.

Bumpy / BumpyZ
--------------
| |since_itg|
| |column_specific_available|

Moves the arrows higher and lower (on the z axis) periodically.

This also enables depth testing.

BumpyOffset / BumpyZOffset
^^^^^^^^^^^^^^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available|

Note that column specific BumpyOffset only modifies the corresponding column specific Bumpy, and not general Bumpy

BumpyPeriod / BumpyZPeriod
^^^^^^^^^^^^^^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available|

BumpySize / BumpyZSize
^^^^^^^^^^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

BumpyX
------
| |since_notitg_v1|
| |column_specific_available|

BumpyXOffset
^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available|

BumpyXPeriod
^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available|

BumpyXSize
^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

BumpyY
------
| |since_notitg_v3|
| |column_specific_available|

BumpyYOffset
^^^^^^^^^^^^
| |since_notitg_v3|
| |column_specific_available|

BumpyYPeriod
^^^^^^^^^^^^
| |since_notitg_v3|
| |column_specific_available|

BumpyYSize
^^^^^^^^^^
| |since_notitg_v3|
| |column_specific_available_since_notitg_v4_2_0|

C%
--------
|since_itg|

Sets the scroll speed to use cMod at a given speed.

Centered / Converge
-------------------
|since_itg|

| Moves the playfield into the vertical center of the screen.
| Percentages beyond ``100%`` will move the playfield past the center.

Centered2
---------
|since_notitg_v1|

Centers the arrows following the arrowpaths.

ClearAll
--------
|since_itg|

Disables all mods, and resets splines if Player.NoClearSplines() is set to false.

Confusion / ConfusionZ
----------------------
|since_notitg_v1|

The notes and receptors spin

ConfusionOffset / ConfusionZOffset
----------------------------------
| |since_notitg_v1|
| |column_specific_available|

Offset the rotation for the Confusion mod

Mod percentage is in radians multiplied by 100

ConfusionX
----------
|since_notitg_v1|

ConfusionXOffset
----------------
| |since_notitg_v1|
| |column_specific_available|

ConfusionY
----------
|since_notitg_v1|

ConfusionYOffset
----------------
| |since_notitg_v1|
| |column_specific_available|

CosClip
-------
|since_notitg_v2|

CouplesMirror
-------------
| |since_notitg_v3|
| |turn_mod|

CouplesSwapNotes
----------------
| |since_notitg_v4|
| |turn_mod|

Cover
-----
|since_itg|

Darkens the area behind the notefield, hiding the background.

Cross
-----
|since_itg|

| Like `Reverse`_, but only affecting the middle two columns.
| Negative percentages affect the outer two columns instead.

| For more complicated mods, consider using column specific Reverse instead.
| Test this and similar mods at https://fms-cat.com/flip-invert/

CubicX
------
|since_notitg_v2|

CubicXOffset
^^^^^^^^^^^^
|since_notitg_v2|

CubicY
------
|since_notitg_v2|

CubicYOffset
^^^^^^^^^^^^
|since_notitg_v2|

CubicZ
------
|since_notitg_v2|

CubicZOffset
^^^^^^^^^^^^
|since_notitg_v2|

Dark
----
| |since_itg|
| |column_specific_available|

| Hides the receptors, while keeping the judgement flashes.
| It has no visible effect until 50%.

Diffuse|%|%|%
-------------
|since_notitg_v1|

Digital
-------
|since_notitg_v1|

DigitalOffset
^^^^^^^^^^^^^
|since_notitg_v1|

DigitalPeriod
^^^^^^^^^^^^^
|since_notitg_v1|

DigitalSteps
^^^^^^^^^^^^
|since_notitg_v1|

DigitalZ
--------
|since_notitg_v1|

DigitalZOffset
^^^^^^^^^^^^^^
|since_notitg_v1|

DigitalZPeriod
^^^^^^^^^^^^^^
|since_notitg_v1|

DigitalZSteps
^^^^^^^^^^^^^
|since_notitg_v1|

DisableMines
------------
|since_notitg_v1|

Distant
-------
|since_itg|

A perspective modifier, where the notefield is tilted away from the player.

Dizzy
-----
| |since_itg|
| |column_specific_available_since_notitg_v4_2_0|

Spins tap notes on the z axis as they approach the receptors.

DizzyHolds
----------
|since_notitg_v1|

Makes hold heads behave like regular tap notes with mods like Dizzy or Confusion

DrawDistance
------------
|since_notitg_v1|

DrawDistanceBack
----------------
|since_notitg_v1|

DrawDistanceFront
-----------------
|since_notitg_v1|

DrawSize
--------
|since_notitg_v1|

Determines how far back the arrows start drawing

DrawSizeBack
------------
|since_notitg_v1|

DrawSizeFront
-------------
|since_notitg_v1|

Drunk
-----
| |since_itg|
| |column_specific_available_since_notitg_v4_2_0|

Moves the receptors and arrows left and right (on the x axis) periodically.

DrunkOffset
^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

DrunkPeriod
^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

DrunkSize
^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

DrunkSpacing
^^^^^^^^^^^^
| |since_notitg_v4|
| |column_specific_available_since_notitg_v4_2_0|

DrunkSpeed
^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

DrunkY
------
| |since_notitg_v3|
| |column_specific_available_since_notitg_v4_2_0|

DrunkYOffset
^^^^^^^^^^^^
| |since_notitg_v3|
| |column_specific_available_since_notitg_v4_2_0|

DrunkYPeriod
^^^^^^^^^^^^
| |since_notitg_v3|
| |column_specific_available_since_notitg_v4_2_0|

DrunkYSize
^^^^^^^^^^
| |since_notitg_v3|
| |column_specific_available_since_notitg_v4_2_0|

DrunkYSpacing
^^^^^^^^^^^^^
| |since_notitg_v4|
| |column_specific_available_since_notitg_v4_2_0|

DrunkYSpeed
^^^^^^^^^^^
| |since_notitg_v3|
| |column_specific_available_since_notitg_v4_2_0|

DrunkZ
------
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

DrunkZOffset
^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

DrunkZPeriod
^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

DrunkZSize
^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

DrunkZSpacing
^^^^^^^^^^^^^
| |since_notitg_v4|
| |column_specific_available_since_notitg_v4_2_0|

DrunkZSpeed
^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

Echo
----
|since_itg|

Adds 8th notes in the same column as the last played note.

Expand
------
|since_itg|

Squashes and stretches the speed of the arrows periodically.

ExpandPeriod
^^^^^^^^^^^^
|since_notitg_v1|

ExpandSize
^^^^^^^^^^
|since_notitg_v1|

FadeBottom|%|%|%
----------------
|since_notitg_v1|

FadeBottomOffset|%|%|%
^^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

FadeTop|%|%|%
-------------
|since_notitg_v1|

FadeTopOffset|%|%|%
^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

FallX|%|%|%
-----------
| |since_notitg_v3|
| |column_specific_available|

FallY|%|%|%
-----------
| |since_notitg_v3|
| |column_specific_available|

FallZ|%|%|%
-----------
| |since_notitg_v3|
| |column_specific_available|

Flip
----
|since_itg|

Flips all four columns horizontally.

Values between ``0%`` and ``50%`` can be used to change the spacing between receptors, keeping the column order the
same;

``50%`` flip places all the four receptors on top of each other;

and values between ``50%`` and ``100%`` can be used to change the spacing between receptors, with the order being
flipped.

https://fms-cat.com/flip-invert/

Floored
-------
|since_itg|

GayHolds
--------
| |since_notitg_v2|
| |column_specific_available|

The opposite of `StraightHolds`_

GlitchyTan / CoSec
------------------
|since_notitg_v2|

GlobalModTimer / ModTimer / Timer
---------------------------------
|since_notitg_v1|

By default, some mods (like ``Drunk``) operate on a global timer, meaning stuff can look slightly different between
plays of a chart. ``GlobalModTimer`` makes these mods use the song position instead of the global timer, so that the mod will
look the exact same each time (Eg: so ``Drunk`` will look the exact same 15 seconds into a song every time).

GlobalModTimerMult / ModTimerMult / TimerMult
---------------------------------------------
|since_notitg_v1|

GlobalModTimerOffset / ModTimerOffset / TimerOffset
---------------------------------------------------
|since_notitg_v1|

Grain / Granulate
-----------------
|since_notitg_v1|

Hallway
-------
|since_itg|

A notefield perspective modifier, where the notefield is tilted towards the player.

Hidden
------
|since_itg|

Flashes arrows (white by default) and hides them as they approach the receptors

HiddenOffset
^^^^^^^^^^^^
|since_itg|

HiddenBlue / HiddenB
--------------------
|since_notitg_v1|

HiddenBlueOffset / HiddenBO
^^^^^^^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

HiddenGreen / HiddenG
---------------------
|since_notitg_v1|

HiddenGreenOffset / HiddenGO
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

HiddenRed / HiddenR
-------------------
|since_notitg_v1|

HiddenRedOffset / HiddenRO
^^^^^^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

Hide
----
| |since_notitg_v2|
| |tween_as_mod|

HideHolds
---------
| |since_notitg_v2|
| |column_specific_available|

Hides the hold bit of hold notes

HideMines
---------
| |since_notitg_v2|
| |column_specific_available|

Hides mines

HideNoteFlash / HideNoteFlashes
-------------------------------
| |since_notitg_v3|
| |column_specific_available|

Hides the blue/yellow/green/etc. receptor flashes when arrows are hit

HoldCull
--------
|since_notitg_v2|

HoldGirth
---------
| |since_notitg_v3|
| |column_specific_available|

HoldStealth
-----------
| |since_notitg_v2|
| |column_specific_available|

Like Steath but on hold tails.

HoldType / SpiralHolds
----------------------
|since_notitg_v1|

A different hold renderer (very noticable on the Y axis)

HoldsToRolls
------------
|since_itg|

HoldTiny
--------
| |since_notitg_v3|
| |column_specific_available|

Incoming
--------
|since_itg|

A notefield perspective modifier like `Hallway`_, but with additional skew towards the screen center.

Invert
------
|since_itg|

Pulls the outer two columns to the middle, pulls the inner two columns to the outer

https://fms-cat.com/flip-invert/

Judgescale / Timing
-------------------
|since_itg|

Left
----
| |since_itg|
| |turn_mod|

Permutes the columns, causing the player to play the chart as though they had turned to the left.

- Left -> Down
- Down -> Right
- Up -> Left
- Right -> Up

Little
------
|since_itg|

LongHolds / LongBoy / LongBoys
------------------------------
|since_notitg_v1|

Makes holds appear longer/shorter than they actually are

``100% LongHolds`` makes holds appear to be double their length, ``-100% LongHolds`` hides holds entirely.

M%
--
|since_itg|

Sets the scroll speed to use MMod at a given speed

ManualNoteFlash / CustomNoteFlash / HideNotePress / HideNotePresses
-------------------------------------------------------------------
| |since_notitg_v3|
| |column_specific_available_since_notitg_v4_2_0|

Hides note presses

MetaDizzy
---------
| |since_notitg_v4_2_0|
| |metamod|

MetaFlip
--------
| |since_notitg_v4_2_0|
| |metamod|

MetaInvert
----------
| |since_notitg_v4_2_0|
| |metamod|

MetaOrient
----------
| |since_notitg_v4_2_0|
| |metamod|

MetaReverse
-----------
| |since_notitg_v4_2_0|
| |metamod|

MetaStealth
-----------
| |since_notitg_v4_2_0|
| |metamod|

Mines
-----
|since_itg|

MineStealth
-----------
| |since_notitg_v2|
| |column_specific_available|

Like Stealth but only on mines.

Mini
----
|since_itg|

Shrinks/grows the playfield

``100% Mini`` makes the playfield half size, ``200% Mini`` makes the playfield zero sized, negative values make the
playfield larger.

Mirror
------
| |since_itg|
| |turn_mod|

Permutes the columns, swapping Left and Right, and Up and Down.

- Left -> Right
- Down -> Up
- Up -> Down
- Right -> Left

MoveX
-----
| |since_notitg_v1|
| |column_specific_available|

Moves the playfield in the X direction. ``100% MoveX`` moves the playfield by the size of the arrow sprite (64 pixels).

MoveY
-----
| |since_notitg_v1|
| |column_specific_available|

Moves the playfield in the Y direction. ``100% MoveY`` moves the playfield by the size of the arrow sprite (64 pixels).

MoveZ
-----
| |since_notitg_v1|
| |column_specific_available|

Moves the playfield in the Z direction. ``100% MoveZ`` moves the playfield by the size of the arrow sprite (64 pixels).

NoHands
-------
|since_itg|

NoHoldJudge / Halgun
--------------------
|since_notitg_v3|

Hides hold judgements. (Yeah! and NG)

NoHolds / NoFreeze
------------------
|since_itg|

Removes holds in the chart.

NoJumps
-------
|since_itg|

Removes all apperances of 2 arrows at the same time.

NoMines
-------
|since_itg|

Removes all mines

NoQuads
-------
|since_itg|

Removes all apperances of 4 arrows at the same time.

NoRolls
-------
|since_itg|

Removes all rolls in the chart.

NoStretch
---------
|since_itg|

What counts as a strech?

NoteSkew / NoteSkewX
--------------------
| |since_notitg_v3|
| |column_specific_available|

Skews notes / receptors in the x axis.

NoteSkewY
---------
| |since_notitg_v4|
| |column_specific_available|

Skews notes / receptors in the y axis.

NoteSkewType
------------
|since_notitg_v3|

Noteskin
--------
|since_itg|

Orient
------
|since_notitg_v4|

Additionally rotates arrows in the direction of travel relative to "upwards".

It can also be used in percentages, to increase or decrease or even invert the effect.

For downwards scroll (e.g. with Reverse or splines), combine this mod with 314% ConfusionOffset

Since |notitg_v4_2_0|, ``Orient`` now reorients itself when when reverse and SCAR mods are enabled.  If you have used
Orient+Reverse before, setting 314% ConfusionOffset is no longer required.

OrientOffset
^^^^^^^^^^^^
|since_notitg_v4_2_0|

It changes the direction the `Orient` mod should reference

NoReorient
^^^^^^^^^^
|since_notitg_v4_2_0|

Disables the `Orient` behavior optimized for reverse and SCAR families

Overhead
--------
|since_itg|

The default perspective modifer, where the notefield is not tilted.

ParabolaX
---------
|since_notitg_v1|

ParabolaXOffset
^^^^^^^^^^^^^^^
|since_notitg_v2|

ParabolaY
---------
|since_notitg_v1|

ParabolaYOffset
^^^^^^^^^^^^^^^
|since_notitg_v2|

ParabolaZ
---------
|since_notitg_v1|

ParabolaZOffset
^^^^^^^^^^^^^^^
|since_notitg_v2|

Passmark
--------
|since_itg|

PlannedomSpeed
--------------
| |since_notitg_v4_2_0|
| |column_specific_available|

Planted
-------
|since_itg|

Pulse
-----
|since_notitg_v1|

PulseInner
----------
|since_notitg_v1|

PulseOuter
----------
|since_notitg_v1|

PulseOffset
-----------
|since_notitg_v1|

PulsePeriod
-----------
|since_notitg_v1|

Quick
-----
|since_itg|

Random
------
|since_itg|

Randomize
---------
|since_notitg_v4_2_0|

Randomize shuffles the notes while they are within the “invisible” region created by Vanish.

RandomizeOffset
^^^^^^^^^^^^^^^
|since_notitg_v4_2_0|

Adjusts the location where `Randomize` takes effect.

RandomSpeed
-----------
|since_itg|

RandomVanish
------------
|since_itg|

A combination of the `Randomize` and `Vanish` mods.

RandomVanishOffset
^^^^^^^^^^^^^^^^^^
|since_notitg_v4_2_0|

Controls both `RandomizeOffset` and `VanishOffset`

ReceptorZBuffer
---------------
| |since_notitg_v3|
| |column_specific_available|

Reverse
-------
| |since_itg|
| |column_specific_available|

Pulls all four receptors to the bottom of the playfield and makes the arrows come down from the top

https://fms-cat.com/flip-invert/

ReverseType
^^^^^^^^^^^
|since_notitg_v1|

Allows reverse to go beyond 100% values.  (without ReverseType, 130% reverse will look identical to 70% reverse)

Right
-----
| |since_itg|
| |turn_mod|

Permutes the columns, causing the player to play the chart as though they had turned to the right.

- Left -> Up
- Down -> Left
- Up -> Right
- Right -> Down

Roll
----
| |since_itg|
| |column_specific_available_since_notitg_v4_2_0|

The notes spin in their lanes around the X axis.

RotationX
---------
| |since_notitg_v2|
| |tween_as_mod|

Rotates the playfield around the x axis.

Units are in degrees.

RotationY
---------
| |since_notitg_v2|
| |tween_as_mod|

Rotates the playfield around the y axis.

Units are in degrees.

RotationZ
---------
| |since_notitg_v2|
| |tween_as_mod|

Rotates the playfield around the z axis.

Units are in degrees.

Sawtooth
--------
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

SawtoothOffset
^^^^^^^^^^^^^^
| |since_notitg_v4|
| |column_specific_available_since_notitg_v4_2_0|

SawtoothPeriod
^^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

SawtoothSize
^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

SawtoothZ
---------
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

SawtoothZOffset
^^^^^^^^^^^^^^^
| |since_notitg_v4|
| |column_specific_available_since_notitg_v4_2_0|

SawtoothZPeriod
^^^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

SawtoothZSize
^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

ScreenFilter
------------
|since_notitg_unk|

ScrollSpeedMult
---------------
| |since_notitg_v4_2_0|
| |column_specific_available|

Multipies the speed mod of a single column.

Default value is 100%. 50% is half-speed.


ShrinkLinear
------------
|since_notitg_v1|

ShrinkLinearX
-------------
|since_notitg_v2|

ShrinkLinearY
-------------
|since_notitg_v2|

ShrinkLinearZ
-------------
|since_notitg_v2|

ShrinkMult
----------
|since_notitg_v1|

ShrinkMultX
-----------
|since_notitg_v2|

ShrinkMultY
-----------
|since_notitg_v2|

ShrinkMultZ
-----------
|since_notitg_v2|

Shuffle
-------
| |since_itg|
| |turn_mod|

SinClip
-------
|since_notitg_v2|

SkewType
--------
|since_notitg_v3|

SkewX
-----
| |since_notitg_v2|
| |tween_as_mod|

SkewY
-----
| |since_notitg_v4|
| |tween_as_mod|

Skippy
------
|since_itg|

SmartBlender
------------
| |since_notitg_v3|
| |turn_mod|

Randomly changes chart notedata, in a way that does not cause doublesteps and preserves some chart properties, such as timing of jacks and drills.

SmartBlender will never output a file with crossovers or footswitches.

SoftShuffle
-----------
| |since_notitg_v2|
| |turn_mod|

Randomly applies SwapLeftRight and SwapUpDown, with 4 possible resulting charts.

Space
-----
|since_itg|

A notefield perspective modifier like `Distant`_, but with additional skew towards the screen center.

SpiralX
-------
|since_notitg_v1|

SpiralXOffset
^^^^^^^^^^^^^
|since_notitg_v2|

SpiralXPeriod
^^^^^^^^^^^^^
|since_notitg_v1|

SpiralY
-------
|since_notitg_v1|

SpiralYOffset
^^^^^^^^^^^^^
|since_notitg_v2|

SpiralYPeriod
^^^^^^^^^^^^^
|since_notitg_v1|

SpiralZ
-------
|since_notitg_v1|

SpiralZOffset
^^^^^^^^^^^^^
|since_notitg_v2|

SpiralZPeriod
^^^^^^^^^^^^^
|since_notitg_v1|

SplineRotX#
-----------
| |since_notitg_v1|
| |spline_column_specific|

SplineRotXOffset#
^^^^^^^^^^^^^^^^^
| |since_notitg_v1|
| |spline_column_specific|

SplineRotXReset
^^^^^^^^^^^^^^^
| |since_notitg_v3|
| |spline_column_specific|

SplineRotXType
^^^^^^^^^^^^^^
|since_notitg_v3|

SplineRotY#
-----------
| |since_notitg_v1|
| |spline_column_specific|

SplineRotYOffset#
^^^^^^^^^^^^^^^^^
| |since_notitg_v1|
| |spline_column_specific|

SplineRotYReset
^^^^^^^^^^^^^^^
| |since_notitg_v3|
| |spline_column_specific|

SplineRotYType
^^^^^^^^^^^^^^
|since_notitg_v3|

SplineRotZ#
-----------
| |since_notitg_v1|
| |spline_column_specific|

SplineRotZOffset#
^^^^^^^^^^^^^^^^^
| |since_notitg_v1|
| |spline_column_specific|

SplineRotZReset
^^^^^^^^^^^^^^^
| |since_notitg_v3|
| |spline_column_specific|

SplineRotZType
^^^^^^^^^^^^^^
|since_notitg_v3|

SplineSkew#
-----------
| |since_notitg_v3|
| |spline_column_specific|

SplineSkewOffset#
^^^^^^^^^^^^^^^^^
| |since_notitg_v3|
| |spline_column_specific|

SplineSkewReset
^^^^^^^^^^^^^^^
| |since_notitg_v3|
| |spline_column_specific|

SplineSkewType
^^^^^^^^^^^^^^
|since_notitg_v3|

SplineStealth#
--------------
| |since_notitg_v2|
| |spline_column_specific|

SplineStealthOffset#
^^^^^^^^^^^^^^^^^^^^
| |since_notitg_v2|
| |spline_column_specific|

SplineStealthReset
^^^^^^^^^^^^^^^^^^
| |since_notitg_v3|
| |spline_column_specific|

SplineStealthType
^^^^^^^^^^^^^^^^^
|since_notitg_v3|

SplineTiny# / SplineSize# / SplineZoom#
---------------------------------------
| |since_notitg_v2|
| |spline_column_specific|

SplineTinyOffset# / SplineSizeOffset# / SplineZoomOffset#
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| |since_notitg_v2|
| |spline_column_specific|

SplineTinyReset / SplineSizeReset / SplineZoomReset
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| |since_notitg_v3|
| |spline_column_specific|

SplineTinyType / SplineSizeType / SplineZoomType
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v3|

SplineX#
--------
| |since_notitg_v1|
| |spline_column_specific|

SplineXOffset#
^^^^^^^^^^^^^^
| |since_notitg_v1|
| |spline_column_specific|

SplineXReset
^^^^^^^^^^^^
| |since_notitg_v3|
| |spline_column_specific|

SplineXType
^^^^^^^^^^^
|since_notitg_v3|

SplineY#
--------
| |since_notitg_v1|
| |spline_column_specific|

SplineYOffset#
^^^^^^^^^^^^^^
| |since_notitg_v1|
| |spline_column_specific|

SplineYReset
^^^^^^^^^^^^
| |since_notitg_v3|
| |spline_column_specific|

SplineYType
^^^^^^^^^^^
|since_notitg_v3|

SplineZ#
--------
| |since_notitg_v1|
| |spline_column_specific|

SplineZOffset#
^^^^^^^^^^^^^^
| |since_notitg_v1|
| |spline_column_specific|

SplineZReset
^^^^^^^^^^^^
| |since_notitg_v3|
| |spline_column_specific|

SplineZType
^^^^^^^^^^^
|since_notitg_v3|

Split
-----
|since_itg|

(For more complicated mods, consider using column specific Reverse instead)

Reverses the rightmost two columns

https://fms-cat.com/flip-invert/

SpookyShuffle
-------------
| |since_notitg_v2|
| |turn_mod|

Square
------
|since_notitg_v1|

SquareOffset
^^^^^^^^^^^^
|since_notitg_v1|

SquarePeriod
^^^^^^^^^^^^
|since_notitg_v1|

SquareZ
-------
|since_notitg_v1|

SquareZOffset
^^^^^^^^^^^^^
|since_notitg_v1|

SquareZPeriod
^^^^^^^^^^^^^
|since_notitg_v1|

Stealth
-------
| |since_itg|
| |column_specific_available|

Hides the arrows. 50% turns the arrows completely white. 100% makes the arrows completely hidden.

Arrows not hit are still visible after passing the receptors (see `StealthPastReceptors`_)

StealthBlue / StealthB
----------------------
|since_notitg_v1|

StealthGreen / StealthG
-----------------------
|since_notitg_v1|

StealthRed / StealthR
---------------------
|since_notitg_v1|

StealthGlow|%|%|%
-----------------
| |since_notitg_v1|
| |column_specific_available|

StealthGlowBlue / StealthGB
---------------------------
|since_notitg_v1|

StealthGlowGreen / StealthGG
----------------------------
|since_notitg_v1|

StealthGlowRed / StealthGR
--------------------------
|since_notitg_v1|

StealthPastReceptors
--------------------
|since_notitg_v1|

If you have Stealth enabled (or a stealth spline), by default arrows that aren't hit will appear again once they fly
past the receptors - this makes them not do that

StealthType
-----------
|since_notitg_v1|

Stomp
-----
|since_itg|

StraightHolds
-------------
| |since_notitg_v2|
| |column_specific_available|

SubtractScore
-------------
|since_itg|

Sudden
------
|since_itg|

Makes arrows appear on the notefield closer to the receptors than usual

SuddenOffset
^^^^^^^^^^^^
|since_itg|

Toy around with different percentage values to adjust how far away arrows appear with the Sudden mod

Stealth splines are easier if you want to be exact

SuddenBlue / SuddenB
--------------------
|since_notitg_v1|

SuddenBlueOffset / SuddenBO
^^^^^^^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

SuddenGreen / SuddenG
---------------------
|since_notitg_v1|

SuddenGreenOffset / SuddenGO
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

SuddenRed / SuddenR
-------------------
|since_notitg_v1|

SuddenRedOffset / SuddenRO
^^^^^^^^^^^^^^^^^^^^^^^^^^
|since_notitg_v1|

SuperShuffle
------------
| |since_itg|
| |turn_mod|

SwapLeftRight
-------------
| |since_notitg_v1|
| |turn_mod|

Permutes the columns, swapping the left and right arrows.

- Left -> Right
- Down -> Down
- Up -> Up
- Right -> Left

SwapSides
---------
| |since_notitg_v3|
| |turn_mod|

SwapUpDown
----------
| |since_notitg_v1|
| |turn_mod|

Permutes the columns, swapping the up and down arrows.

- Left -> Left
- Down -> Up
- Up -> Down
- Right -> Right

TanBumpy / TanBumpyZ
--------------------
| |since_notitg_v1|
| |column_specific_available|

TanBumpyOffset / TanBumpyZOffset
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TanBumpyPeriod / TanBumpyZPeriod
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TanBumpySize / TanBumpyZSize
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TanBumpyX
---------
| |since_notitg_v1|
| |column_specific_available|

TanBumpyXOffset
^^^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TanBumpyXPeriod
^^^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TanBumpyXSize
^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TanBumpyY
---------
| |since_notitg_v3|
| |column_specific_available|

TanBumpyYOffset
^^^^^^^^^^^^^^^
| |since_notitg_v3|
| |column_specific_available_since_notitg_v4_2_0|

TanBumpyYPeriod
^^^^^^^^^^^^^^^
| |since_notitg_v3|
| |column_specific_available_since_notitg_v4_2_0|

TanBumpyYSize
^^^^^^^^^^^^^
| |since_notitg_v3|
| |column_specific_available_since_notitg_v4_2_0|

TanClip
-------
|since_notitg_v2|

TanDigital
----------
|since_notitg_v1|

TanDigitalOffset
^^^^^^^^^^^^^^^^
|since_notitg_v1|

TanDigitalPeriod
^^^^^^^^^^^^^^^^
|since_notitg_v1|

TanDigitalSteps
^^^^^^^^^^^^^^^
|since_notitg_v1|

TanDigitalZ
-----------
|since_notitg_v1|

TanDigitalZOffset
^^^^^^^^^^^^^^^^^
|since_notitg_v1|

TanDigitalZPeriod
^^^^^^^^^^^^^^^^^
|since_notitg_v1|

TanDigitalZSteps
^^^^^^^^^^^^^^^^
|since_notitg_v1|

TanDrunk
--------
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TanDrunkOffset
^^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TanDrunkPeriod
^^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TanDrunkSize
^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TanDrunkSpacing
^^^^^^^^^^^^^^^
| |since_notitg_v4|
| |column_specific_available_since_notitg_v4_2_0|

TanDrunkSpeed
^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TanDrunkY
---------
| |since_notitg_v3|
| |column_specific_available_since_notitg_v4_2_0|

TanDrunkYOffset
^^^^^^^^^^^^^^^
| |since_notitg_v3|
| |column_specific_available_since_notitg_v4_2_0|

TanDrunkYPeriod
^^^^^^^^^^^^^^^
| |since_notitg_v3|
| |column_specific_available_since_notitg_v4_2_0|

TanDrunkYSize
^^^^^^^^^^^^^
| |since_notitg_v3|
| |column_specific_available_since_notitg_v4_2_0|

TanDrunkYSpacing
^^^^^^^^^^^^^^^^
| |since_notitg_v4|
| |column_specific_available_since_notitg_v4_2_0|

TanDrunkYSpeed
^^^^^^^^^^^^^^
| |since_notitg_v3|
| |column_specific_available_since_notitg_v4_2_0|

TanDrunkZ
---------
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TanDrunkZOffset
^^^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TanDrunkZPeriod
^^^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TanDrunkZSize
^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TanDrunkZSpacing
^^^^^^^^^^^^^^^^
| |since_notitg_v4|
| |column_specific_available_since_notitg_v4_2_0|

TanDrunkZSpeed
^^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TanExpand
---------
|since_notitg_v1|

TanExpandPeriod
^^^^^^^^^^^^^^^
|since_notitg_v1|

TanExpandSize
^^^^^^^^^^^^^
|since_notitg_v1|

TanPulse
--------
|since_notitg_v2|

TanPulseInner
-------------
|since_notitg_v2|

TanPulseOuter
-------------
|since_notitg_v2|

TanPulseOffset
--------------
|since_notitg_v2|

TanPulsePeriod
--------------
|since_notitg_v2|

TanTipsy
--------
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TanTipsyOffset
^^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TanTipsySpacing
^^^^^^^^^^^^^^^
| |since_notitg_v4|
| |column_specific_available_since_notitg_v4_2_0|

TanTipsySpeed
^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TanTornado
----------
|since_notitg_v1|

TanTornadoOffset
^^^^^^^^^^^^^^^^
|since_notitg_v1|

TanTornadoPeriod
^^^^^^^^^^^^^^^^
|since_notitg_v1|

TanTornadoZ
-----------
|since_notitg_v1|

TanTornadoZOffset
^^^^^^^^^^^^^^^^^
|since_notitg_v1|

TanTornadoZPeriod
^^^^^^^^^^^^^^^^^
|since_notitg_v1|

TextureFilterOff
----------------
| |since_notitg_v3|
| |column_specific_available|

Tiny
----
| |since_notitg_v1|
| |column_specific_available|

Reduces the size of the arrows, without changing their position relative to each other like mini does.  200% will cause the arrows to dissapear entirely.

TinyX
-----
| |since_notitg_v2|
| |column_specific_available|

Reduces the size of the arrows in the x direction.

TinyY
-----
| |since_notitg_v2|
| |column_specific_available|

Reduces the size of the arrows in the x direction.

TinyZ
-----
| |since_notitg_v2|
| |column_specific_available|

Reduces the size of the arrows in the x direction.

Tipsy
-----
| |since_itg|
| |column_specific_available_since_notitg_v4_2_0|

TipsyOffset
^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

TipsySpacing
^^^^^^^^^^^^
| |since_notitg_v4|
| |column_specific_available_since_notitg_v4_2_0|

TipsySpeed
^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

Tornado
-------
|since_itg|

The notes fly in from the left, to the right, as they move towards the receptors

TornadoOffset
^^^^^^^^^^^^^
|since_notitg_v1|

TornadoPeriod
^^^^^^^^^^^^^
|since_notitg_v1|

TornadoZ
--------
|since_notitg_v1|

TornadoZOffset
^^^^^^^^^^^^^^
|since_notitg_v1|

TornadoZPeriod
^^^^^^^^^^^^^^
|since_notitg_v1|

Turn
----
|since_itg|

Twirl
-----
| |since_itg|
| |column_specific_available_since_notitg_v4_2_0|

The notes and their holds spin in 3D (around the Y/up axis)

This creates a nice twisting appearance for the holds

Twister
-------
|since_itg|

Vanish
------
|since_notitg_v4_2_0|

Vanish makes the arrows disappear for a bit at a spot in the middle of the screen.

VanishOffset
^^^^^^^^^^^^
|since_notitg_v4_2_0|

Adjusts the location where `Vanish` takes effect.

VanishSize
^^^^^^^^^^
|since_notitg_v4_2_0|

Wave
----
| |since_itg|
| |column_specific_available|

WaveOffset
^^^^^^^^^^
| |since_notitg_v4|
| |column_specific_available|

WavePeriod
^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available|

WaveSize
^^^^^^^^
|since_notitg_v1|

Wide
----
|since_itg|

WireFrame
---------
| |since_notitg_v3|
| |column_specific_available|

WireFrameGirth
^^^^^^^^^^^^^^
| |since_notitg_v3|
| |column_specific_available|

WireFrameWidth
^^^^^^^^^^^^^^
| |since_notitg_v3|
| |column_specific_available|

X
-
| |since_notitg_v2|
| |tween_as_mod|

Moves the playfield in the x direction.

Y
-
| |since_notitg_v2|
| |tween_as_mod|

Moves the playfield in the y direction.

Z
-
| |since_notitg_v2|
| |tween_as_mod|

Moves the playfield in the z direction.

ZBuffer
-------
|since_notitg_v1|

Makes arrows/holds read from and write to the depth/Z buffer

Zigzag
------
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

ZigzagOffset
^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

ZigzagPeriod
^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

ZigzagSize
^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

ZigzagZ
-------
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

ZigzagZOffset
^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

ZigzagZPeriod
^^^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

ZigzagZSize
^^^^^^^^^^^
| |since_notitg_v1|
| |column_specific_available_since_notitg_v4_2_0|

Zoom
----
| |since_notitg_v2|
| |tween_as_mod|

ZoomX
-----
| |since_notitg_v2|
| |tween_as_mod|

ZoomY
-----
| |since_notitg_v2|
| |tween_as_mod|

ZoomZ
-----
| |since_notitg_v2|
| |tween_as_mod|

ZTest
-----
|since_notitg_unk|

.. |column_start| replace:: |receptor_16| Column-specific variant available
.. |column_normal_end| replace:: (add the column number, 0 indexed, to the end of the mod name)
.. |column_spline_end| replace:: (add the column number, 0 indexed, after the word ``spline``)

.. |column_specific_available| replace:: |column_start| |column_normal_end|
.. |column_specific_available_since_notitg_v4_2_0| replace:: |column_start| since |notitg_v4_2_0| |column_normal_end|
.. |spline_column_specific| replace:: |column_start| |column_spline_end|

.. |turn_mod| replace:: (This mod is a Turn mod, and directly affects the player's notedata. It cannot be applied mid-file.)
.. |metamod| replace:: (This mod is a metamod, cannot be applied by a simfile, and will disqualify your score.)
.. |tween_as_mod| replace:: |tween| as mod
