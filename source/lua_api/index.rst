Lua API
=======

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   actor
   actor_frame
   actor_frame_texture
   actor_proxy
   actor_scroller
   actor_sound
   bitmap_text
   course
   difficulty_meter
   fading_banner
   game
   game_sound_manager
   game_state
   help_display
   high_score
   high_score_list
   memory_card_manager
   message_manager
   model
   note_skin_manager
   player
   player_stage_stats
   polygon
   prefs_manager
   profile
   profile_manager
   radar_values
   rage_display
   rage_file_manager
   rage_input
   rage_shader_program
   rage_sound
   rage_texture
   rage_texture_render_target
   screen_gameplay
   screen_manager
   song
   song_manager
   sprite
   stage_stats
   stats_manager
   steps
   theme_manager
   trail
   unlock_manager

   enumerations
   globals
