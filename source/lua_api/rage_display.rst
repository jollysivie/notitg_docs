RageDisplay
===========

|since_notitg_v1|

.. contents:: :local:

Description
-----------

The game window, accessible from the global :lua:attr:`DISPLAY <global.global.DISPLAY>`

API reference
-------------

.. lua:autoclass:: RageDisplay
