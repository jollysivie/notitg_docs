Sprite
======

|since_itg|

.. contents:: :local:

Description
-----------

A sprite is an actor with a texture

XML attributes
--------------

**File**

A filepath to the initial image the sprite should use

API reference
-------------

.. lua:autoclass:: Sprite
