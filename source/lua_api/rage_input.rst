RageInput
=========

|since_itg|

.. contents:: :local:

Description
-----------

The input manager singleton, accessible from the global :lua:attr:`INPUTMAN <global.global.INPUTMAN>`

API reference
-------------

.. lua:autoclass:: RageInput

