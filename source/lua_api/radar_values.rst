RadarValues
===========

|since_itg|

.. contents:: :local:

Description
-----------

Various computed difficulty values

API reference
-------------

.. lua:autoclass:: RadarValues
