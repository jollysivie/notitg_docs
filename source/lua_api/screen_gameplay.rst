ScreenGameplay
==============

|since_itg|

.. contents:: :local:

Description
-----------

The screen used during main gameplay - accessible with ``SCREENMAN:GetTopScreen()`` during gameplay.

API reference
-------------

.. lua:autoclass:: ScreenGameplay
